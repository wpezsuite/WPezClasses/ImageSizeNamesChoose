<?php

namespace WPezSuite\WPezClasses\ImageSizeNamesChoose;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

class ClassImageSizeNamesChoose implements InterfaceImageSizeNamesChoose {

	protected $_arr_imgs;
	protected $_arr_defaults;
	protected $_arr_defaults_names_choose;
	protected $_str_names_choose_add_where;
	protected $_arr_names_choose_remove;
	protected $_int_content_width_count;
	protected $_int_content_width;
	protected $_int_content_width_temp;
	protected $_bool_content_width_compare;

	public function __construct() {

		$this->setPropertyDefaults();

	}

	protected function setPropertyDefaults() {

		$this->_arr_imgs                   = [];
		$this->_arr_defaults               = [
			'active' => true,
			'name'   => false,
			'width'  => 0,
			'height' => 0,
			'crop'   => false
		];
		$this->_arr_defaults_names_choose  = [
			'active'                         => true,
			'option'                         => false,
			'content_width_compare_override' => false
		];
		$this->_str_names_choose_add_where = 'after';
		$this->_arr_names_choose_remove    = [];
		$this->_int_content_width_count    = 0;
		$this->_int_content_width          = 0;
		$this->_int_content_width_temp     = false;
		$this->_bool_content_width_compare = false;
	}

	public function setAddWhere( $str = 'after' ) {

		$this->_str_names_choose_add_where = $str;

	}

	/*
	* array = [name1 => bool, name2 => bool,...];
	* remove if bool === true
	*/
	public function setRemoveNames( $arr = false ) {

		if ( is_array( $arr ) ) {

			$this->_arr_names_choose_remove = $arr;

			return true;
		}

		return false;
	}

	/**
	 * We actually override the WP default and default to int_content_width =
	 * 0.
	 *
	 * Ref: https://codex.wordpress.org/Content_Width
	 * Ref:
	 * https://wycks.wordpress.com/2013/02/14/why-the-content_width-wordpress-global-kinda-sucks/
	 *
	 * @param int $int
	 */
	public function setContentWidth( $int = false ) {

		// using the method but not passing a value will use the global $content_width value
		if ( $int === false ) {
			$this->_int_content_width = false;

		} else {
			$this->_int_content_width = absint( $int );
		}
	}

	/**
	 * Note: If set to true, any *custom* (read: add_image_size) image with a
	 * width > $content_width will NOT be added via the image_size_names_choose
	 * filter. The default setting of false will not do any sort of checking.
	 *
	 * @param bool $bool
	 */
	public function setContentWidthCompare( $bool = false ) {

		$this->_bool_content_width_compare = (bool) $bool;
	}


	public function updateDefaults( $arr_defaults = false ) {

		if ( is_array( $arr_defaults ) ) {

			$this->_arr_defaults = array_merge( $this->_arr_defaults, $arr_defaults );

		}
	}

	public function updateDefaultsNamesChoose( $arr_defaults = false ) {

		if ( is_array( $arr_defaults ) ) {

			$this->_arr_defaults = array_merge( $this->_arr_defaults, $arr_defaults );

		}
	}


	public function pushImage( $arr_args = false ) {

		if ( is_array( $arr_args ) ) {

			$this->_arr_imgs[] = $arr_args;

			return true;
		}

		return false;
	}


	public function loadImages( $arr_images = false ) {

		if ( is_array( $arr_images ) ) {
			$arr_ret = [];
			foreach ( $arr_images as $str_ndx => $arr_args ) {

				$arr_ret[ $str_ndx ] = $this->pushImage( $arr_args );

			}

			return $arr_ret;
		}

		return false;
	}


	public function filterImageSizeNamesChoose( $arr_sizes ) {

		$arr_names_choose = [];

		// we want this - at least for now - only for the first time this filter fires
		if ( $this->_int_content_width_count == 0 ) {

			$this->_int_content_width_count ++;

			global $content_width;

			$this->_int_content_width_temp = $content_width;
			if ( ! is_bool( $this->_int_content_width ) ) {
				$content_width = $this->_int_content_width;
			}

			foreach ( $this->_arr_imgs as $key => $arr_img ) {

				if ( ! is_array( $arr_img ) ) {
					continue;
				}

				$arr_img = array_merge( $this->_arr_defaults, $arr_img );
				if ( $arr_img['active'] === false ) {
					continue;
				}

				if ( isset( $arr_img['names_choose'] ) && is_array( $arr_img['names_choose'] ) ) {
					$arr_img['names_choose'] = array_merge( $this->_arr_defaults_names_choose, $arr_img['names_choose'] );

					if ( $arr_img['names_choose']['active'] === false ) {
						continue;
					}
				} else {
					$arr_img['names_choose'] = false;
				}

				// do we want to add this one or not?
				// - should we compare the content width?
				// - ok compare the image width with the content width
				// - does the image override the content width compare?
				if ( $this->_bool_content_width_compare === true && $arr_img['width'] > $content_width && ( isset( $arr_img['names_choose']['content_width_compare_override'] ) && $arr_img['names_choose']['content_width_compare_override'] !== true ) ) {
					continue;
				}
				if ( $arr_img['names_choose'] === false || ( isset( $arr_img['names_choose']['option'] ) && ! is_string( $arr_img['names_choose']['option'] ) ) ) {

					// the basic default is to use the image size name
					$arr_names_choose[ $arr_img['name'] ] = $arr_img['name'];
				} elseif ( is_string( $arr_img['names_choose']['option'] ) ) {

					// if we have a ['names_choose']['option'] then use it!
					$arr_names_choose[ $arr_img['name'] ] = $arr_img['names_choose']['option'];
				}
			}

		} else {

			// flip the content width back to what it was for anything downstream
			if ( $this->_int_content_width_temp !== false ) {
				global $content_width;
				$content_width = $this->_int_content_width_temp;
			}

		}

		$arr_ret = [];
		switch ( $this->_str_names_choose_add_where ) {

			case 'before':
				$arr_ret = array_merge( $arr_names_choose, $arr_sizes );
				break;

			case 'replace':
				$arr_ret = $arr_names_choose;
				break;

			case 'after':
			default:
				$arr_ret = array_merge( $arr_sizes, $arr_names_choose );
				break;
		}

		/*
		 * array = [name1 => bool, name2 => bool,...];
		 * remove if bool === true
		 */
		foreach ( $this->_arr_names_choose_remove as $str_name => $bool_active ) {
			if ( $bool_active === true ) {
				unset( $arr_ret[ $str_name ] );
			}
		}

		return $arr_ret;

	}

}